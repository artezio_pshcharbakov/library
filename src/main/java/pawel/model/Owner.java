package pawel.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "owner", uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})})
public class Owner extends Person {

    @Enumerated
    private Department dept;

    @OneToMany(mappedBy = "reader")
    private Set<Book> books = new HashSet<>();

    public Owner() {
    }

    public Department getDept() {
        return dept;
    }

    public void setDept(Department dept) {
        this.dept = dept;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    //todo move to service method
    private void add(Book book) {
        books.add(book);
        book.setDateOfIssue(LocalDate.now());
    }

    @Override
    public String toString() {
        return super.toString() + "Owner{" + "dept=" + dept + '}';
    }
}
