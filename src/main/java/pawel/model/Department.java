package pawel.model;

public enum Department {

    FBA("Факультет бизнес-администрирования"),

    FBT("Факультет бизнес-технологий"),

    FHSB("Факультет «Высшая школа бизнеса»"),

    FATR("Факультет повышения квалификации и переподготовки");

    private String description;

    Department(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Department{" +
                "description='" + description + '\'' +
                '}';
    }
}
