package pawel.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

@Entity
@Table(name = "book", uniqueConstraints = {@UniqueConstraint(columnNames = {"book_id"})})
public class Book {

    @Id
    @Column(name = "book_id", nullable = false, unique = true)
    private String guid;

    @Column(name = "title", length = 45, nullable = false)
    private String title;

    @ManyToMany(mappedBy = "books")
    private Set<Author> authors = new HashSet<>();

    //private ScienceCategory category;

    @Column(name = "description", length = 900)
    private String description;

    @Column(name = "issue")
    //Hibernate 5 good work with Data and Time API. No reason another annotation
    private LocalDate dateOfIssue;

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, targetEntity = Owner.class)
    @JoinColumn(name = "fk_owner")
    private Owner reader;

    private String isbn;

    public Book() {
        guid = UUID.randomUUID().toString();
    }

    /*private Book(Builder builder) {
        title = builder.title;
    }

    public static class Builder {
        private String title;

        private Set<Author> authors;

        public Builder() {
        }

        public Builder title(String name) {
            title = name;
            return this;
        }

        public Book build() {
            return new Book(this);
        }
    }*/

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getReader() {
        return reader;
    }

    public void setReader(Owner reader) {
        this.reader = reader;
    }

    public LocalDate getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(LocalDate dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).
                append(title).
                append(description).
                toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Book rhs = (Book) obj;
        return new EqualsBuilder()
                .append(guid, rhs.getGuid())
                .append(title, rhs.getTitle())
                .append(description, rhs.getDescription())
                .isEquals();
    }
}
