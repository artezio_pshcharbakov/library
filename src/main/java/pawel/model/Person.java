package pawel.model;

import javax.persistence.*;
import java.util.UUID;

//@MappedSuperclass //todo later
@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Person {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    private String giud;

    @Column(name = "name", length = 45, nullable = false)
    private String name;

    @Column(name = "surname", length = 45, nullable = false)
    private String surname;

    @Column(name = "patronymic", length = 45)
    private String patronymic;

    public Person() {
        giud = UUID.randomUUID().toString();
    }

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getGiud() {
        return giud;
    }

    public void setGiud(String giud) {
        this.giud = giud;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Person{" +
                "giud='" + giud + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                '}';
    }
}
