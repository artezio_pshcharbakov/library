package pawel.model;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "author", uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})})
public class Author extends Person{

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "book_author",
            joinColumns = {@JoinColumn(name = "author_fk")},
            inverseJoinColumns = {@JoinColumn(name = "book_fk")}
    )
    private Set<Book> books = new HashSet<>();

    public Author() {
        super();
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        return books != null ? books.equals(author.books) : author.books == null;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).
                append(getGiud()).
                append(getName()).
                append(getSurname()).
                append(getPatronymic()).
                toHashCode();
    }
}
