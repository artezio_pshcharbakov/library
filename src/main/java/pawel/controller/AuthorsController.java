package pawel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pawel.dao.AuthorDaoService;
import pawel.model.Author;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/authors")
public class AuthorsController {

    @Autowired
    private AuthorDaoService authorDaoService;

    private List<Author> writers = new ArrayList<>();

    private Author single;

    //@RequestMapping(method = RequestMethod.GET) prior Spring 4.3
    @GetMapping
    public String showAllAuthors(Model model) {
        writers = authorDaoService.findAll();
        writers.forEach(author -> model.addAttribute(author.getName(), author));
        model.addAttribute("writers", writers);
        return "authors";
    }
}
