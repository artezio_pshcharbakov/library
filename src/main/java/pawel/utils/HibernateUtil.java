package pawel.utils;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import pawel.model.Author;
import pawel.model.Book;
import pawel.model.Owner;

@Service
public class HibernateUtil {

    @Value("${hibernate.config.file}")
    private  String hibernateConfig;

    private HibernateUtil() {
    }

    @Bean
    public SessionFactory getSessionFactory() {
        try {
            Configuration configuration = new Configuration().configure(hibernateConfig);
            configuration.addAnnotatedClass(Author.class)
            .addAnnotatedClass(Book.class)
            .addAnnotatedClass(Owner.class);

            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties());

            return configuration.buildSessionFactory(builder.build());
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}

