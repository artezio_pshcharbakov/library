package pawel.dao;

import org.hibernate.MultiIdentifierLoadAccess;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pawel.model.Author;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AuthorDaoSession {

    @Autowired(required = false)
    private SessionFactory factory;

    public Author findById(String guid) {
        return factory.openSession().get(Author.class, guid);
    }

    public void save(Author author) {
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(author);
        tx.commit();
        session.close();
    }

    public void update(Author author) {
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(author);
        tx.commit();
        session.close();
    }

    public void delete (Author author) {
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(author);
        tx.commit();
        session.close();
    }

    public Set<Author> findAll() {
        Set<Author> authors = new HashSet<>(factory.openSession().createQuery("From " + Author.class.getSimpleName()).list());
        /*feature Hibernate 5 return a Stream
        Stream<Author> auth = factory.openSession().createQuery("SELECT a FROM author a", Author.class).stream();*/
        return authors;
    }

    //feature from 5 version
    public List<Author> findMultipleId(String... idForFind) {
        Session session = factory.openSession();
        MultiIdentifierLoadAccess<Author> multi = session.byMultipleIds(Author.class);
        return multi.multiLoad(idForFind);
    }
}
