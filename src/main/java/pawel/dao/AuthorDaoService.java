package pawel.dao;

import org.springframework.stereotype.Service;
import pawel.model.Author;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AuthorDaoService {

    @PersistenceContext
    private EntityManager manager;

    @PostConstruct
    public void init() throws Exception{
        if(manager == null) {
            throw new Exception("Manager is absents a bean!!!");
        }
    }

    public Author findById(String guid) {
        return manager.find(Author.class, guid);
    }

    public void save(Author author) {
        manager.persist(author);
    }

    public void update(Author author) {
        manager.merge(author);
    }

    public void delete (Author author) {
        manager.remove(author);
    }

    public List<Author> findAll() {
        CriteriaQuery<Author> criteriaQuery = manager.getCriteriaBuilder()
                .createQuery(Author.class);
        Root<Author> root = criteriaQuery.from(Author.class);
        criteriaQuery.select(root);
        return  manager.createQuery(criteriaQuery).getResultList();
    }
}
