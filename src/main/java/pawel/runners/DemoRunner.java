package pawel.runners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pawel.dao.AuthorDaoService;
import pawel.model.Author;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication(scanBasePackages = {"pawel.dao", "pawel.utils", "pawel.controller"})
public class DemoRunner implements CommandLineRunner {

    @Autowired
    private AuthorDaoService authorDaoService;

    private Author author = new Author();

    private List<Author> writers = new ArrayList<>();

    public static void main(String[] args) {
        SpringApplication.run(DemoRunner.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        author.setName("Carl");
        author.setPatronymic("Clara");
        author.setSurname("Kalary");
        //see result in libtest DB, table author
        authorDaoService.save(author);
        //see on http://localhost:9090/ with update name
        author = authorDaoService.findById("229fd704-1b4f-4198-abf9-60ceca68956e");
        author.setName("Gans");
        authorDaoService.update(author);
    }
}
