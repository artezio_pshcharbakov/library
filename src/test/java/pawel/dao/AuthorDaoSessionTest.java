package pawel.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import pawel.model.Author;
import pawel.model.Book;
import pawel.utils.HibernateUtil;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = {"pawel.dao"},
        basePackageClasses = HibernateUtil.class)
@SpringBootTest(classes = AuthorDaoSession.class)
public class AuthorDaoSessionTest {

    @Autowired
    private AuthorDaoSession dao;

    private Book book;
    private Author author;
    private Author secondA;

    {
        book = new Book();
        book.setTitle("Hibernate quick");
        book.setDescription("Small guide for access Hibernate");
        dao = new AuthorDaoSession();

        author = new Author();
        author.setGiud("1");
        author.setName("Hans");
        author.setSurname("Andersen");
        author.setPatronymic("Sweden");
        author.setBooks(Collections.singleton(book));
        book.setAuthors(Collections.singleton(author));

        secondA = new Author();
        secondA.setGiud("2");
        secondA.setName("Pavel");
        secondA.setSurname("Shcharbakov");
        secondA.setPatronymic("Alex");
    }

    @Before
    public void init() {
        dao.save(author);
        dao.save(secondA);
    }

    @After
    public void clear() {
        dao.delete(author);
        dao.delete(secondA);
    }

    @Test
    public void save() {
        dao.delete(author);
        dao.delete(secondA);

        dao.save(author);

        Author findAuthor = dao.findById(author.getGiud());
        assertEquals(findAuthor.getGiud(), author.getGiud());
        assertEquals(findAuthor.getSurname(), author.getSurname());
        assertTrue(findAuthor.getBooks().containsAll(author.getBooks()));
    }

    @Test
    public void findById() {
        Author exceptAuthor = dao.findById("1");
        assertEquals(exceptAuthor.getGiud(), author.getGiud());
        assertEquals(exceptAuthor.getName(), author.getName());
        assertEquals(exceptAuthor.getSurname(), author.getSurname());
        assertEquals(exceptAuthor.getPatronymic(), author.getPatronymic());
        assertTrue(exceptAuthor.getBooks().containsAll(author.getBooks()));

        Author exceptSecond = dao.findById("2");
        assertEquals(exceptSecond.getGiud(), secondA.getGiud());
        assertEquals(exceptSecond.getName(), secondA.getName());
        assertEquals(exceptSecond.getSurname(), secondA.getSurname());
        assertEquals(exceptSecond.getPatronymic(), secondA.getPatronymic());
        assertTrue(exceptSecond.getBooks().containsAll(secondA.getBooks()));
    }

    @Test
    public void delete() {
        assertNotNull(dao.findById(author.getGiud()));
        assertNotNull(dao.findById(secondA.getGiud()));

        dao.delete(author);
        dao.delete(secondA);

        assertNull(dao.findById(author.getGiud()));
        assertNull(dao.findById(secondA.getGiud()));
    }

    @Test
    public void findAll() {
        Set<Author> all = dao.findAll();
        assertEquals(2, all.size());
    }

    @Test
    public void findMultipleId() {
        String[] authorsIds = {"1", "2"};
        List<Author> authors = dao.findMultipleId(authorsIds);

        assertEquals(2, authors.size());

        Author first = authors.get(0);
        Author second = authors.get(1);

        assertEquals(first.getName(), author.getName());
        assertEquals(first.getSurname(), author.getSurname());
        assertEquals(second.getName(), secondA.getName());
        assertEquals(second.getSurname(), secondA.getSurname());
    }
}