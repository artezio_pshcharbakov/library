package pawel.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import pawel.model.Book;
import pawel.utils.HibernateUtil;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = {"pawel.dao"},
        basePackageClasses = HibernateUtil.class)
@SpringBootTest(classes = AuthorDaoService.class)
public class AuthorDaoServiceTest {

    @Autowired
    private EntityManager manager;

    @Test
    public void findById() {
    }

    @Test
    public void save() {
        manager.persist(new Book());
    }

    @Test
    public void update() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void findAll() {
    }
}